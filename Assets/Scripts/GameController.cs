﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car;

public enum GameState
{
	NotStarted,
	InProgress,
	Complete
}

public class GameController : MonoBehaviour {

	private long msPrevious;
	private long msHideMessage = long.MaxValue;

	private GameObject player;
	private GameObject hud;
	private Text hudSpeed;
	private Text hudTime1;
	private Text hudTime2;
	private CarController snowmobileController;
	private CarUserControl snowmobileUserController;
	private int frame = 0;
	private string cheat = "";
	private DateTime lastTime = DateTime.Now.AddDays(1);

	[SerializeField] private AudioClip[] m_CountdownClips = new AudioClip[4];

	[SerializeField] private RaceStatsController m_RaceStats;
	[SerializeField] private GameObject m_Tutorial;
	[SerializeField] private AudioClip m_CountdownMusic;
	[SerializeField] private AudioClip m_RaceMusic;
	[SerializeField] private Speedometer Speedometer;
	[SerializeField] private Minimap Minimap;
	[SerializeField] private Messager Messager;
	[SerializeField] private MusicController Music;

	private GameObject lastRespawnGate = null;

	public GameState RaceState { get; private set; }
	public bool CanRespawnPlayer { get; private set; }
	public TimeSpan RawRaceTime { get; private set; }
	public int Collisions { get;  set; }
	public TimeSpan TimePenalties { get; private set; }
	public TimeSpan FinalTime { get; private set; }

	public void CompleteRace()
	{
		if (this.RaceState == GameState.Complete) {
			return;
		}
		Debug.Log ("Race complete!");
		this.RaceState = GameState.Complete;
		this.Music.Stop ();
		this.SetHudVisibility (false);
		this.m_Tutorial.SetActive (false);
		this.TimePenalties = TimeSpan.FromSeconds (5 * this.Collisions);
		this.FinalTime = RawRaceTime.Add (this.TimePenalties);

		this.m_RaceStats.Show();
	}

	public void SetRespawnGate(GameObject gate)
	{
		this.lastRespawnGate = gate;
	}

	public void SetHudVisibility(bool visible)
	{
		this.hud.SetActive (visible);
	}

	public void RespawnPlayer()
	{
		if (this.CanRespawnPlayer) 
		{
			RespawnAtGate(this.lastRespawnGate);
		}
	}

	public void RecordCollision()
	{
		this.ShowMessage ("Collision: +5s");
		this.Collisions++;
	}

	// Use this for initialization
	void Start () {
		this.RaceState = GameState.NotStarted;
		this.RawRaceTime = TimeSpan.FromDays(-999);
		this.hud = GameObject.Find ("HUD");
		this.hudTime1 = GameObject.Find("TimeValueMMSS").GetComponent<Text>();
		this.hudTime2 = GameObject.Find("TimeValueFF").GetComponent<Text>();

		this.player = GameObject.FindGameObjectWithTag ("Player");
		this.snowmobileController = this.player.GetComponent<CarController>();
		this.snowmobileUserController = this.player.GetComponent<CarUserControl>();
		this.CanRespawnPlayer = true;

		this.SetHudVisibility (true);
		this.ShowMessage ("");
	}

	// Update is called once per frame
	void Update () {
		if (this.frame == 1) {
			this.Initialize ();
		}

		var now = DateTime.Now;
		var frameMilliseconds = Mathf.Clamp((float)(now - lastTime).TotalMilliseconds, 0, 1000);
		this.lastTime = now;

		this.RawRaceTime += TimeSpan.FromMilliseconds(frameMilliseconds * Time.timeScale);

		var ms = (int)this.RawRaceTime.TotalMilliseconds;

		if (ms >= -3000 && msPrevious < -3000) {
			this.ShowMessage ("3...");	
			this.PlaySound (m_CountdownClips [3]);
		} else if (ms >= -2000 && msPrevious < -2000) {
			this.ShowMessage ("2...");	
			this.PlaySound (m_CountdownClips [2]);
		} else if (ms >= -1000 && msPrevious < -1000) {
			this.ShowMessage ("1...");	
			this.PlaySound (m_CountdownClips [1]);
		} else if (ms >= 0 && msPrevious < 0) {
			this.ShowMessage ("GO!");	
			this.Music.Play(m_RaceMusic, true);
			this.PlaySound (m_CountdownClips [0]);
		}

		SetTime(Math.Max(ms, 0));

		if (this.RaceState == GameState.NotStarted && ms >= 0) {
			this.RaceState = GameState.InProgress;
		}

		this.snowmobileUserController.DrivingEnabled = this.RaceState == GameState.InProgress;
		var speed = Math.Abs(this.snowmobileController.CurrentSpeed);
		this.SetSpeed(speed);
		this.UpdateMinimap();

		#if !MOBILE_INPUT
		//if (!Application.isMobilePlatform) {
		if (Input.GetKey (KeyCode.Space)) {
			Debug.Log ("Respawn Player");
			this.RespawnPlayer ();
		} else {
			this.CanRespawnPlayer = true;
		}

		if (Input.GetKeyUp(KeyCode.G))
		{
			this.cheat = "g";
		}
		else if (Input.GetKeyUp(KeyCode.Alpha0))
		{
			this.cheat += "0";
		}
		else if (Input.GetKeyUp(KeyCode.Alpha1))
		{
			this.cheat += "1";
		}
		else if (Input.GetKeyUp(KeyCode.Alpha2))
		{
			this.cheat += "2";
		}
		else if (Input.GetKeyUp(KeyCode.Alpha3))
		{
			this.cheat += "3";
		}
		else if (Input.GetKeyUp(KeyCode.Alpha4))
		{
			this.cheat += "4";
		}
		else if (Input.GetKeyUp(KeyCode.Alpha5))
		{
			this.cheat += "5";
		}
		else if (Input.GetKeyUp(KeyCode.Alpha6))
		{
			this.cheat += "6";
		}
		else if (Input.GetKeyUp(KeyCode.Alpha7))
		{
			this.cheat += "7";
		}
		else if (Input.GetKeyUp(KeyCode.Alpha8))
		{
			this.cheat += "8";
		}
		else if (Input.GetKeyUp(KeyCode.Alpha9))
		{
			this.cheat += "9";
		}

		if (this.cheat.StartsWith("g") && this.cheat.Length == 3)
		{
			var gate = GameObject.Find("RespawnGate" + this.cheat.Substring(1));
			if (gate != null)
			{
				RespawnAtGate(gate.transform.GetChild(0).gameObject);
			}
			this.cheat = "";
		}
		//}
		#endif

		// Hide old messages.
		if (ms > this.msHideMessage) {
			this.ShowMessage("");
			this.msHideMessage = long.MaxValue;
		}

		this.msPrevious = ms;
		frame++;
	}

	private void Initialize()
	{
		this.m_Tutorial.SetActive (true);
		var tutorialController = this.m_Tutorial.GetComponent<TutorialController> ();
		tutorialController.OnClosed += (object sender, EventArgs e) => 
		{
			this.StartCountdown();
		};

		this.m_RaceStats.gameObject.SetActive (false);
	}

	private void RespawnAtGate(GameObject gate)
	{
		if (gate != null) 
		{
			var playerPoint = gate.transform.GetChild (0);
			this.player.transform.position = playerPoint.transform.position;
			this.player.transform.rotation = playerPoint.transform.rotation;
			this.player.GetComponent<Rigidbody> ().velocity = new Vector3 ();
			this.player.GetComponent<Rigidbody> ().angularVelocity = new Vector3 ();
		}
	}

	private void StartCountdown()
	{
		this.PlaySound (m_CountdownMusic);
		this.RawRaceTime = TimeSpan.FromSeconds (-m_CountdownMusic.length);
	}

	public void ShowMessage(string message)
	{
		this.Messager.ShowMessage(message);
		this.msHideMessage = this.msPrevious + 3000;
	}

	// sets up and adds new audio source to the gane object
	private AudioSource PlaySound(AudioClip clip)
	{
		// create the new audio source component on the game object and set up its properties
		AudioSource source = gameObject.AddComponent<AudioSource>();
		source.clip = clip;
		//source.volume = 0;
		//source.loop = true;

		// start the clip from a random point
		//source.time = Random.Range(0f, clip.length);
		source.Play();
		source.minDistance = 5;
		source.maxDistance = 100;
		source.dopplerLevel = 0;
		return source;
	}

	private void SetTime(int ms)
	{
		int minutes = ms / 60000;
		int seconds = (ms / 1000) % 60;
		int cs = (ms / 10) % 100;

		this.hudTime1.text = string.Format("{0:00}:{1:00}", minutes, seconds);
		this.hudTime2.text = string.Format(".{0:00}", cs);
	}

	private void SetSpeed(float mph)
	{
		if (this.Speedometer != null) {
			this.Speedometer.SetSpeed (mph);
		}
	}

	private void UpdateMinimap()
	{
		this.Minimap.SetMyIcon (this.player.transform.position, this.player.transform.eulerAngles.y);
	}
}

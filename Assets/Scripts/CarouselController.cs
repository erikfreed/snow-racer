﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarouselController : MonoBehaviour {

	private float SecondsPerRevolution = 30;
	private DateTime timeLast;

	// Use this for initialization
	void Start () {
		timeLast = DateTime.Now;
	}
	
	// Update is called once per frame
	void Update () {
		var now = DateTime.Now;
		gameObject.transform.Rotate (0, (float)(now.Subtract(timeLast).TotalSeconds * 360 / SecondsPerRevolution), 0);
		timeLast = now;
	}
}

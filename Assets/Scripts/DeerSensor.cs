﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DeerSensorType {
	LeftEar,
	RightEar,
	Proximity,
	Jump,
}

public class DeerSensor : MonoBehaviour {

	private DeerController controller;

	[SerializeField]
	private DeerSensorType sensorType;

	// Use this for initialization
	void Start () {
		controller = this.gameObject.GetComponentInParent<DeerController> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) 
	{
		var gate = other.gameObject;

		if (gate.tag == "PlayerTrip") {
			Debug.Log ("Dear sensor " + this.sensorType + " detected " + gate.tag);

			switch (this.sensorType)
			{
			case DeerSensorType.LeftEar:
				controller.LookLeft ();
				break;
			case DeerSensorType.Proximity:
				controller.Run ();
				break;
			}
		}
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour 
{
	[SerializeField] private GameObject[] Pages = new GameObject[] {};
	[SerializeField] private GameObject NextButton = null;
	[SerializeField] private Text TipTitle = null;
	[SerializeField] private AudioClip MusicClip;
	[SerializeField] private MusicController MusicController;

	public event EventHandler OnClosed = delegate {};

	private int currentPage;

	private bool initialized = false;

	void Update () {
		if (initialized) {
			return;
		}
		initialized = true;

		ShowPage (0);
		for (var i = 1; i < Pages.Length; i++) {
			HidePage (i);
		}

		this.MusicController.Play (this.MusicClip, true);
	}

	public void NextPage()
	{
		if (currentPage < Pages.Length - 1) {
			Pages [currentPage].SetActive (false);
			ShowPage (currentPage + 1);
		}
	}

	public void CloseTutorial()
	{
		this.MusicController.Stop ();
		this.gameObject.SetActive (false);
		if (this.OnClosed != null) {
			this.OnClosed (this, null);
		}
	}

	private void HidePage(int index)
	{
		Pages [index].SetActive (false);
	}

	private void ShowPage(int index)
	{
		Pages [index].SetActive (true);
		currentPage = index;
		TipTitle.text = string.Format(
			"TIP {0} of {1}",
			currentPage + 1,
			Pages.Length);

		if (NextButton != null) {
			NextButton.SetActive (index < Pages.Length - 1);
		}


	}
}

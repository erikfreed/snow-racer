﻿using System;
using UnityEngine;

public class DeerController : MonoBehaviour 
{

	private Animator anim;
	private GameObject leftEar;
	private int hashLookLeft;
	private int hashRun;
	private int hashRunState;
	private DateTime timeLastFrame;
	[SerializeField] private ParticleSystem[] jumpParticles;
	private DateTime kickTime = DateTime.Now.AddYears(1);

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		hashLookLeft = Animator.StringToHash("HeardToLeft");
		hashRun = Animator.StringToHash("Run");
		hashRunState = Animator.StringToHash("Base Layer.Run");
	}
	
	// Update is called once per frame
	void Update () {
		var now = DateTime.Now;
		var timeDelta = (float)(now - timeLastFrame).TotalSeconds;
		var state = anim.GetCurrentAnimatorStateInfo (0);
		if (state.fullPathHash == hashRunState) {
			//Debug.Log ("running");

			if (timeDelta > 0 && timeDelta < 10)
			{
				var body = this.gameObject.GetComponentInParent<Rigidbody> ();

				var azi = body.rotation.eulerAngles.y * Mathf.PI / 180f;
				var target = new Vector3 (Mathf.Sin(azi), 0, Mathf.Cos(azi)) * 10;
				var force = (target - body.velocity) * body.mass / timeDelta;
				var atPosition = GameObject.Find ("Thrust").transform.position;
				body.AddForceAtPosition(force, atPosition);
			}
		}
		timeLastFrame = now;

		if (now.Ticks > kickTime.Ticks) {
			EmitJumpParticles ();
			kickTime = now.AddYears (1);
		}
			
	}

	public void LookLeft()
	{
		this.anim.SetTrigger(this.hashLookLeft);
	}

	public void Run()
	{
		this.anim.SetTrigger(this.hashRun);
		kickTime = DateTime.Now.AddMilliseconds(150);
	}

	void OnTriggerEnter(Collider other) 
	{
		Debug.Log ("Dear heard something");
		var gate = other.gameObject;

		Debug.Log ("Dear heard " + gate.tag);
		if (gate.tag == "PlayerTrip") {
			
		}
	}

	private void EmitJumpParticles()
	{
		foreach (var particles in this.jumpParticles) {
			Debug.Log ("Emit " + particles.gameObject.name);
			particles.Emit (20);
		}
	}

}

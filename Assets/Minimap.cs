﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour {

	[SerializeField] private RectTransform myIcon;
	[SerializeField] private float west;
	[SerializeField] private float east;
	[SerializeField] private float south;
	[SerializeField] private float north;

	private RectTransform mapBounds;

	// Use this for initialization
	void Start () {
		mapBounds = this.gameObject.GetComponent<RectTransform> ();
	}

	// Update is called once per frame
	public void SetMyIcon (Vector3 myPosition, float heading) {
		var pos = new Vector2 (
			(myPosition.x - west) / (east - west) * mapBounds.rect.width, 
			(myPosition.z - south) / (north - south) * mapBounds.rect.height);
		this.myIcon.anchoredPosition = pos;
		this.myIcon.rotation = Quaternion.Euler (0, 0, -heading);
	}
}

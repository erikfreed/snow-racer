﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class CameraController : MonoBehaviour {
	[SerializeField] public CarController snowmobile;
	[SerializeField] public Transform camPivot;

	private bool cameraDisplaced = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//var snowTransform = snowmobile.gameObject.transform;
		//this.gameObject.transform.localPosition = new Vector3 (0, 3, -6);
//		this.gameObject.transform.localRotation = Quaternion.Euler (
//			14-snowTransform.rotation.eulerAngles.x, 
//			0, 
//			-snowTransform.rotation.eulerAngles.z);

//		this.gameObject.transform = Transform.res

//		RaycastHit hit;
//		if(Physics.Raycast(this.gameObject.transform.position + Vector3.up * 10, Vector3.down, out hit, 11)) {
//			this.gameObject.transform.position = hit.point + Vector3.up;
//
//			Debug.Log ("Camera hit ground: " + hit.point);
//		}

		var pivotLocation = camPivot.transform.position;
//		Debug.Log ("pivot: " + pivotLocation);
		RaycastHit hit;
		if (Physics.Raycast (pivotLocation + Vector3.up * 10, Vector3.down, out hit, 11)) {
			this.gameObject.transform.position = hit.point + Vector3.up;

			//Debug.Log ("Camera hit ground: " + hit.point);
			cameraDisplaced = true;
		} else if (cameraDisplaced) {
			this.gameObject.transform.localPosition = new Vector3 ();
			cameraDisplaced = false;
		}

	}
}

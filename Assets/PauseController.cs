﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour {

	// Use this for initialization
	public void TogglePause()
	{
		Time.timeScale = (Time.timeScale > .5) ? 0 : 1;
	}
}

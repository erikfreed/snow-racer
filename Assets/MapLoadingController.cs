﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapLoadingController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}

	[SerializeField] private AudioSource m_Music;

	private bool firstFrame = true;

	private AsyncOperation async = null; // When assigned, load is in progress.

	public void LoadMap(int sceneIndex)
	{
		async = SceneManager.LoadSceneAsync(sceneIndex);
//		SceneManager.sceneLoaded += (arg0, arg1) => {
//			Debug.Log("Load game complete");
//		};
	}

	public void Update()
	{
		// Do this at first update call so music doesn't start too soon before screen appears.
		if (firstFrame) {
			m_Music.Play (20000);
			LoadMap (1);
			this.firstFrame = false;
		}

		if (async != null) {
			Debug.Log ("Load complete: " + async.progress);
		}
	}
}

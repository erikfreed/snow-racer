using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof (CarController))]
    public class CarUserControl : MonoBehaviour
    {
        private CarController m_Car; // the car controller we want to use

		public bool DrivingEnabled { get; set; }

		public void Start()
		{
			this.DrivingEnabled = true;
		}

        private void Awake()
        {
            // get the car controller
            m_Car = GetComponent<CarController>();
        }

        private void FixedUpdate()
        {
			float h = 0;
			float v = 0;
			//float brake = -1;
			if (this.DrivingEnabled) {
				// pass the input to the car!
				h = CrossPlatformInputManager.GetAxis ("Horizontal");
				v = CrossPlatformInputManager.GetAxis ("Vertical");
				//brake = v;
			}
 
			m_Car.Move(h, v, v, 0);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace UnityStandardAssets.Vehicles.Car 
{
	public class WheelContact : MonoBehaviour {
			
		public bool OnGround { get; set; }
		public float SpeedDelta { get; set; }
		private DateTime lastOnGround = DateTime.Now;

		private WheelCollider[] wheels;

		// Use this for initialization
		void Start () {
			this.wheels = this.GetComponentsInChildren<WheelCollider>(false);
			Debug.Log (this.name + ": " + string.Join(",", this.wheels.Select(w => w.name).ToArray()));
		}
		
		// Update is called once per frame
		void Update () {
			var now = DateTime.Now;
			if (this.WheelsOnGround ()) {
				this.lastOnGround = now;
			}
			this.OnGround = (now - this.lastOnGround).TotalMilliseconds < 200;
		}


		private bool WheelsOnGround()
		{
			foreach (var wheel in wheels) {
				WheelHit hitGround;
				wheel.GetGroundHit (out hitGround);

				if (hitGround.normal != Vector3.zero) {
					return true;
				}
			}
			return false;
		}
	}
}
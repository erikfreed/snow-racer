using System;
using UnityEngine;

namespace ThinIce.Water
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(WaterBase))]
    public class GerstnerDisplace : Displace { }
}
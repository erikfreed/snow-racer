﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThinIceController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var sphere = this.gameObject.AddComponent<SphereCollider>();
		sphere.radius = 2;
		sphere.isTrigger = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) 
	{
		var otherObject = other.gameObject;

		if (otherObject.tag == "PlayerTrip") {
			var joint = this.gameObject.GetComponent<FixedJoint> ();
			if (joint != null) {
				joint.breakForce = 1;
			}
		}
	}
}

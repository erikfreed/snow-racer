﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class SnowController : MonoBehaviour {

	[SerializeField] ParticleSystem snowCloud;
	[SerializeField] ParticleSystem snowSpecks;
	//[SerializeField] CarController snowmobile;
	[SerializeField] WheelContact contact;

	// Use this for initialization
	void Start () {
		var cloudEmission = snowCloud.emission;
		cloudEmission.enabled = true;
		var speckEmission = snowSpecks.emission;
		speckEmission.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		var amount = contact.OnGround ? contact.SpeedDelta : 0;
		this.SetSpeed (amount);
	}

	public void SetSpeed(float speed)
	{
		var cloudEmission = snowCloud.emission;
		cloudEmission.rateOverTimeMultiplier = Mathf.Clamp ((speed - 5) * 3, 0, 200);
		var speckEmission = snowSpecks.emission;
		speckEmission.rateOverTimeMultiplier = Mathf.Clamp ((speed - 5) * 3, 0, 200);
	}

//	public void Burst(int count)
//	{
//		var cloudEmission = snowCloud.emission;
//		cloudEmission.SetBursts(new [] { new ParticleSystem.Burst(0, (short)count) });
//		var speckEmission = snowSpecks.emission;
//		speckEmission.SetBursts(new [] { new ParticleSystem.Burst(0, (short)count) });
//	}
}

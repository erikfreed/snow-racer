﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailGateSetter : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var terrains = Terrain.activeTerrains;
		foreach (Transform gateTransform in this.transform) {
			var obj = gateTransform.gameObject;

			////Debug.Log ("Gate: " + obj.name);
			Vector3 pos = new Vector3(gateTransform.position.x, 0, gateTransform.position.z);

			foreach (var terrain in terrains) {
				pos.y = terrain.SampleHeight (pos) + .2f;
				var sw = terrain.GetPosition ();
				var ne = terrain.GetPosition () + new Vector3(500, 0, 500);

				if (pos.x >= sw.x && pos.z >= sw.z && pos.x <= ne.x && pos.z <= ne.z) {
					////Debug.Log ("  Terrain Height: " + terrain.name + ": " + pos.y);
					gateTransform.position = pos;
					break;
				}
			}


		}
	}
	
	// Update is called once per frame
	void Update () {

	}
}

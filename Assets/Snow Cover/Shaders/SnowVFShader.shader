﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/SnowVF" {
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_NoSnow("No Snow", Range(-3,3)) = 0.2
		_FullSnow("Full Snow", Range(-3,3)) = 0.5
	}
	SubShader {
		Pass {

		// 1.) This will be the base forward rendering pass in which ambient, vertex, and
		// main directional light will be applied. Additional lights will need additional passes
		// using the "ForwardAdd" lightmode.
		// see: http://docs.unity3d.com/Manual/SL-PassTags.html
		Tags{ "LightMode" = "ForwardBase" }

		CGPROGRAM

		#pragma vertex vert
		#pragma fragment frag
		#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight

		#include "UnityCG.cginc"
		#include "AutoLight.cginc"
		#include "UnityLightingCommon.cginc"

		sampler2D _MainTex;
		float4 _MainTex_ST;
		float _NoSnow;
		float _FullSnow;

		// vertex input: position, UV
		struct appdata {
			float4 vertex : POSITION;
			float4 uv : TEXCOORD0;
			float3 normal : NORMAL;
		};

		struct v2f {
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
			half3 worldNormal : NORMAL;
			fixed3 diff : COLOR0;
			fixed3 ambient : COLOR1;
			SHADOW_COORDS(1) // put shadows data into TEXCOORD1
		};


		v2f vert(appdata v) {
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
			o.uv = TRANSFORM_TEX(v.uv, _MainTex);//v.texcoord;
			o.worldNormal = UnityObjectToWorldNormal(v.normal);

			half nl = max(0, dot(o.worldNormal, _WorldSpaceLightPos0.xyz));
			o.diff = nl * _LightColor0;
			o.ambient = ShadeSH9(half4(o.worldNormal, 1));
			// compute shadows data
			TRANSFER_SHADOW(o)
			return o;

		}

		fixed4 frag(v2f i) : COLOR{
			// sample the texture
			fixed4 col = tex2D(_MainTex, i.uv);

			// Apply snow cover
			float index = i.worldNormal.y + (col.r + col.g + col.b) / -3;
			float snow = clamp((index - _NoSnow) / (_FullSnow - _NoSnow), 0, 1);
			col.rgb = lerp(col,fixed3(1.3,1.3,1.4),snow);

			// compute shadow attenuation (1.0 = fully lit, 0.0 = fully shadowed)
			fixed shadow = SHADOW_ATTENUATION(i);
			fixed3 lighting = i.diff * shadow + i.ambient;
			col.rgb *= lighting;

			return col;
		}

		ENDCG
		}
	}

	// 7.) To receive or cast a shadow, shaders must implement the appropriate "Shadow Collector" or "Shadow Caster" pass.
	// Although we haven't explicitly done so in this shader, if these passes are missing they will be read from a fallback
	// shader instead, so specify one here to import the collector/caster passes used in that fallback.
	Fallback "VertexLit"
}
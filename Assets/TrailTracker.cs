﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailTracker : MonoBehaviour {

	private GameController gameController;

	[SerializeField] private LapController[] laps;

	private HashSet<GameObject> remainingGates = new HashSet<GameObject> ();

	public delegate void GateTouched(GameObject gate);

	// Use this for initialization
	void Start () {
		this.gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

		var respawnGates = GameObject.FindGameObjectsWithTag ("Respawn");
		foreach (var gate in respawnGates) {
			this.remainingGates.Add (gate);
		}
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("update");
	}

	void OnTriggerEnter(Collider other) 
	{
		var gate = other.gameObject;

		if (gate.tag == "Respawn") {
			Debug.Log (gameObject.name + " triggered respawn gate " + gate.tag);
			this.gameController.SetRespawnGate (gate);
			if (this.remainingGates.Contains (gate)) {
				this.remainingGates.Remove (gate);
			}
			Debug.Log ("Remaining gates: " + this.remainingGates.Count);

			foreach (var lap in laps) {
				lap.MarkGateCrossed(gate);
			}
		} else if (gate.tag == "Finish") {
			Debug.Log (gameObject.name + " triggered finish gate " + gate.tag);
			if (this.remainingGates.Count == 0) {
				this.gameController.CompleteRace ();
			} else {
				foreach (var lap in laps) {
					lap.MarkFinishLineCrossed ();
				}
			}
		}
	}
}

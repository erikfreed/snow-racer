﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Messager : MonoBehaviour {

	[SerializeField] private Text text1;
	[SerializeField] private Text text2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowMessage(string message)
	{
		this.text1.text = message;
		this.text2.text = message;
	}
}

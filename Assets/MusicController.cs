﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {
	AudioSource source;

	// Use this for initialization
	void Start () {
		source = gameObject.GetComponent<AudioSource>();
	}

	public void Play(AudioClip clip, bool loop)
	{
		source.Stop ();
		source = gameObject.GetComponent<AudioSource>();
		source.clip = clip;
		source.loop = loop;
		source.Play();
	}

	public void Stop()
	{
		source.Stop ();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class RaceStatsController : MonoBehaviour {
	[SerializeField] private GameController GameController;
	[SerializeField] private MusicController MusicController;
	[SerializeField] private AudioClip MusicClip;
	[SerializeField] private Text RawTimeText;
	[SerializeField] private Text CollisionsText;
	[SerializeField] private Text PenaltiesText;
	[SerializeField] private Text FinalTimeText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Show()
	{
		this.RawTimeText.text = PrettyTime(this.GameController.RawRaceTime);
		this.PenaltiesText.text = PrettyTime(this.GameController.TimePenalties);
		this.FinalTimeText.text = PrettyTime(this.GameController.FinalTime);
		this.CollisionsText.text = this.GameController.Collisions.ToString();

		this.gameObject.SetActive (true);
		this.MusicController.Play (this.MusicClip, false);
	}

	private static string PrettyTime(TimeSpan time)
	{
		var ms = (int)time.TotalMilliseconds;
		int minutes = ms / 60000;
		int seconds = (ms / 1000) % 60;
		int cs = (ms / 10) % 100;

		return string.Format("{0:00}:{1:00}.{2:00}", minutes, seconds, cs);
	}

	public void CloseRaceStats()
	{
		this.MusicController.Stop ();
		SceneManager.LoadScene(0);
	}
}

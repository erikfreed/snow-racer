﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapController : MonoBehaviour {

	[SerializeField] private GameController GameController;
	[SerializeField] private GameObject[] lapGates;
	[SerializeField] private GameObject lapCompleteBlockage;

	private HashSet<GameObject> remainingGates = new HashSet<GameObject> ();

	public bool IsComplete { get; set; }

	// Use this for initialization
	void Start () {
		foreach (var gate in this.lapGates) {
			// TODO: Find by Respawn Tag
			this.remainingGates.Add (gate.transform.GetChild(0).gameObject);
		}

		this.lapCompleteBlockage.SetActive (false);

		this.IsComplete = false;
	}

	public void MarkGateCrossed(GameObject gate)
	{
		if (this.remainingGates.Contains (gate)) {
			this.remainingGates.Remove (gate);
			Debug.Log (this.gameObject.name + " gates remaining: " + this.remainingGates.Count);
		}
	}

	public void MarkFinishLineCrossed()
	{
		if (remainingGates.Count == 0) {
			// Enable trail blockage to direct rider through different 
			this.lapCompleteBlockage.SetActive (true);
			this.IsComplete = true;
			this.GameController.ShowMessage ("1 LAP TO GO!");
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Speedometer : MonoBehaviour {

	[SerializeField] private Text digital;
	[SerializeField] private RectTransform needle;
	[SerializeField] private float angleMph0;
	[SerializeField] private float angleMph100;

	public void SetSpeed(float mph)
	{
		if (digital != null) {
			digital.text = string.Format ("{0:f0}", mph);
		}

		needle.rotation = Quaternion.Euler(0, 0, angleMph0 + mph * (angleMph100 - angleMph0) / 100f);
	}
}

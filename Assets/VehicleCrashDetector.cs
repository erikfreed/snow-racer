﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleCrashDetector : MonoBehaviour {
	private GameController gameController;
	private AudioSource audioSource;

	[SerializeField] private AudioClip smallCollisionClip;
	[SerializeField] private AudioClip largeCollisionClip;

	// Use this for initialization
	void Start () {
		this.gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		this.audioSource = this.GetComponent<AudioSource> ();
		Debug.Log ("Collision init");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision) {
		var mag = collision.relativeVelocity.magnitude;

		// Don't count collisions from rough terrain on under carriage
		foreach (ContactPoint contact in collision.contacts) {
			if (contact.thisCollider.gameObject.name == "UnderCollider") {
				if (mag > 1) {
					this.PlaySound (smallCollisionClip);
				}
				return;
			}
		}

		if (mag > 5) {
			this.PlaySound (largeCollisionClip);
			this.gameController.RecordCollision ();
		} else if (mag > 1) {
			this.PlaySound (smallCollisionClip);
		}
	}

	private void PlaySound(AudioClip clip)
	{
		this.audioSource.clip = clip;
		this.audioSource.Play ();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnControl : MonoBehaviour {

	private GameController gameController;

	private GameObject respawnButton;

	// Use this for initialization
	void Start () {
		this.gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		this.respawnButton = this.gameObject;// GameObject.Find("TowButton");
	}
	
	// Update is called once per frame
	void Update () {
		this.respawnButton.SetActive (this.gameController.CanRespawnPlayer);
	}

	public void RespawnPlayer()
	{
		this.gameController.RespawnPlayer ();
	}
}

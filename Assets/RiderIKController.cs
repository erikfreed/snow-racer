﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class RiderIKController : MonoBehaviour {

	Animator anim;

	private float weightIK = 1.0f;
	[SerializeField] public Transform leftHandTarget;
	[SerializeField] public Transform rightHandTarget;
	[SerializeField] public Transform leftFootTarget;
	[SerializeField] public Transform rightFootTarget;
	[SerializeField] public Transform lookAtTarget;

	[SerializeField] public Transform leftKneeHint;
	[SerializeField] public Transform rightKneeHint;

	[SerializeField] public CarController snowmobile;
	[SerializeField] public GameObject helmet;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnAnimatorIK()
	{
		// Lean into turns.
		var lean = snowmobile.CurrentSteerAngle * -.5f * Mathf.Min(snowmobile.CurrentSpeed / 50f, 1f);
		this.gameObject.transform.localRotation = Quaternion.Euler(new Vector3 (8, 0, lean));

		anim.SetIKPositionWeight (AvatarIKGoal.LeftHand, weightIK);
		anim.SetIKPosition (AvatarIKGoal.LeftHand, leftHandTarget.position);
		anim.SetIKRotationWeight (AvatarIKGoal.LeftHand, weightIK);
		anim.SetIKRotation (AvatarIKGoal.LeftHand, leftHandTarget.rotation);

		anim.SetIKPositionWeight (AvatarIKGoal.RightHand, weightIK);
		anim.SetIKPosition (AvatarIKGoal.RightHand, rightHandTarget.position);
		anim.SetIKRotationWeight (AvatarIKGoal.RightHand, weightIK);
		anim.SetIKRotation (AvatarIKGoal.RightHand, rightHandTarget.rotation);

		anim.SetIKPositionWeight (AvatarIKGoal.LeftFoot, weightIK);
		anim.SetIKPosition (AvatarIKGoal.LeftFoot, leftFootTarget.position);
		anim.SetIKRotationWeight (AvatarIKGoal.LeftFoot, weightIK);
		anim.SetIKRotation (AvatarIKGoal.LeftFoot, leftFootTarget.rotation);

		anim.SetIKPositionWeight (AvatarIKGoal.RightFoot, weightIK);
		anim.SetIKPosition (AvatarIKGoal.RightFoot, rightFootTarget.position);
		anim.SetIKRotationWeight (AvatarIKGoal.RightFoot, weightIK);
		anim.SetIKRotation (AvatarIKGoal.RightFoot, rightFootTarget.rotation);

		anim.SetIKHintPositionWeight (AvatarIKHint.LeftKnee, weightIK);
		anim.SetIKHintPosition (AvatarIKHint.LeftKnee, leftKneeHint.position);

		anim.SetIKHintPositionWeight (AvatarIKHint.RightKnee, weightIK);
		anim.SetIKHintPosition (AvatarIKHint.RightKnee, rightKneeHint.position);

		anim.SetLookAtPosition (lookAtTarget.position);
		anim.SetLookAtWeight (weightIK);



		var headTransform = anim.GetBoneTransform (HumanBodyBones.Head);

		this.helmet.transform.position = headTransform.position;
		this.helmet.transform.rotation = Quaternion.Euler(headTransform.rotation.eulerAngles + new Vector3(0, snowmobile.CurrentSteerAngle, 0));
	}
}
